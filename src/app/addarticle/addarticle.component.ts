import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ArticleService } from '../article.service';
import { Article } from '../entities';

@Component({
  selector: 'app-addarticle',
  templateUrl: './addarticle.component.html',
  styleUrls: ['./addarticle.component.css']
})
export class AddarticleComponent implements OnInit {

  operation:Article = {
  title:"",
  date:"",
  content:""
  }

  constructor(private addArticle:ArticleService, private router:Router){ }

  ngOnInit(): void {
  }

  addOperation() {
    this.addArticle.add(this.operation).subscribe(() => {
      this.router.navigate(['/']);
    });
  }
}

