import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddarticleComponent } from './addarticle/addarticle.component';
import { ArticleComponent } from './article/article.component';

const routes: Routes = [
  {path: '', component: ArticleComponent},
  {path: 'formulaire', component: AddarticleComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
